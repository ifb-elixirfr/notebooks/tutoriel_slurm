#!/bin/bash
#SBATCH --array=0-5
#SBATCH --cpus-per-task=10
#SBATCH --mem=1G
#SBATCH --time=00:30:00

module load bowtie2/2.4.4 samtools/1.15.1

reference_index="/shared/bank/arabidopsis_thaliana/TAIR10.1/bowtie2/Arabidopsis_thaliana.TAIR10.1_genomic"
files=(data/*fastq.gz)
file_id=$(basename -s .fastq.gz "${files[$SLURM_ARRAY_TASK_ID]}")

mkdir -p results

srun -J "${file_id} bowtie2" bowtie2 --threads="${SLURM_CPUS_PER_TASK}" -x "${reference_index}" -U "data/${file_id}.fastq.gz" -S "results/${file_id}.sam"
srun -J "${file_id} filter" samtools view -hbS -q 30 -o "results/${file_id}.filtered.bam" "results/${file_id}.sam"
srun -J "${file_id} sort" samtools sort -o "results/${file_id}.bam" "results/${file_id}.filtered.bam"

rm "results/${file_id}.sam" "results/${file_id}.filtered.bam"